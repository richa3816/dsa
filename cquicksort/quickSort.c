#include <stdio.h>

void printArr(int *arr, int n)
{
	for (int i = 0; i < n; ++i) printf("%d ", arr[i]);
	printf("\n");
}

void swap(int *a, int *b)
{
	int tmp = *a;
	*a = *b;
	*b = tmp;
}

int quickSort(int *arr, int lowi, int highi)
{
	if (lowi < highi)
	{
		int pivotval = arr[highi];
		int swapi = lowi-1;

		for (int i = lowi; i < highi; ++i)
		{
			if (arr[i] < pivotval)
			{
				++swapi;
				if (swapi < i) swap(&arr[swapi], &arr[i]);
			}
		}
		swap(&arr[++swapi], &arr[highi]);

		quickSort(arr, lowi, swapi-1);
		quickSort(arr, swapi+1, highi);
	}
}

int main()
{
	int arr[] = {3, 2, 5, 0, 1, 8, 7, 6, 9, 4};
	int n = sizeof(arr)/sizeof(*arr);

	quickSort(arr, 0, n-1);
	printArr(arr, n);

	return 0;
}

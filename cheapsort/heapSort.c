#include <stdio.h>

void printArr(int *arr, int n)
{
	for (int i = 0; i < n; ++i) printf("%d ", arr[i]);
	printf("\n");
}

void swap(int *a, int *b)
{
	int tmp = *a;
	*a = *b;
	*b = tmp;
}

void maxHeapify(int *arr, int n, int i)
{
	int maxi = i;
	int lefti = i*2 + 1;
	int righti = i*2 + 2;

	if (lefti < n && arr[maxi] < arr[lefti]) maxi = lefti;
	if (righti < n && arr[maxi] < arr[righti]) maxi = righti;
	if (maxi != i)
	{
		swap(&arr[i], &arr[maxi]);
		maxHeapify(arr, n, maxi);
	}
}

void heapSort(int *arr, int n)
{
	for (int i = n/2 - 1; i >= 0; --i) maxHeapify(arr, n, i);
	for (int i = n-1; i > 0; --i)
	{
		swap(&arr[0], &arr[i]);
		maxHeapify(arr, i, 0);
	}
}

int main()
{
	int arr[] = {3, 2, 5, 0, 1, 8, 7, 6, 9, 4};
	int n = sizeof(arr)/sizeof(*arr);

	heapSort(arr, n);
	printArr(arr, n);
	return 0;
}
